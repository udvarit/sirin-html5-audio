Hey there!

I created this extension so that I can enjoy music on my favourite websites without having to install Flash player. These sites are currently:
http://www.ektoplazm.com/
http://psyradio.com.ua/

Sirin is a Russian mythological creature who sings songs that make people mad - hence the connection to this project. If you'd like to know more, Wikipedia has some wonderful illustrations and lore: https://en.wikipedia.org/wiki/Sirin

If you'd like to see another site html5-audio-fied, please contact me on the project's GitLab page or leave a comment. No promises, but I like a challenge sometimes.

The extension's icon is mashed up from the HTML5 logo by W3C (CC-by 3.0) and Designmodo's speaker logo (CC BY-NC 3.0).

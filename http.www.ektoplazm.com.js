NodeList.prototype.forEach = function(callback){Array.from(this).forEach(callback)};
HTMLCollection.prototype.forEach = function(callback){Array.from(this).forEach(callback)};

var rawSongUrls = document.body.innerHTML.match(/{soundFile:"([^"]+)"}/g);
var songUrls = [];
rawSongUrls.forEach(function(raw){
	var decoded = raw.match(/soundFile:"([^"]*)"/)[1];
	var songsString;
	try {
		songsString = atob(decoded);
	} catch(e) {
		// Dear library that Ektoplazm uses on the backend. Why would you append an extra "A" sometimes to the base64 string?
		songsString = atob(decoded.slice(0, -1));
	}
	var songs = songsString.split(',');
	songUrls = songUrls.concat(songs);
});

var audioNodes = [];
var i = 0;
var lastAudioNode = null;
document.querySelectorAll('.entry.trackbg .tl .n').forEach(function(e){
	var audio = document.createElement("audio");
	audio.controls = true;
	audio.src = songUrls[i++];
	audio.preload = "none";
	
	e.parentNode.insertBefore(audio, e);
	
	audio.onplay = function(){
		audioNodes.forEach(function(e){
			if(e != audio) {
				e.pause();
			}
		});
	};
	
	audio.onvolumechange = function() {
		window.volume = audio.volume;
		window.muted = audio.muted;
	};
	
	audioNodes.push(audio);
	if(lastAudioNode) {
		lastAudioNode.onended = function(){audio.play()};
	}
	lastAudioNode = audio;
});

window.volume = 1;
window.muted = false;
setInterval(function(){
	audioNodes.forEach(function(e){
		e.volume = window.volume;
		e.muted = window.muted;
	});
}, 1000);

browser.runtime.sendMessage({status: "ready", origin: "ektoplazm"});

browser.runtime.onMessage.addListener(msg => {
	if(msg.status == 'ready') {
		browser.tabs.query({active: true, currentWindow: true}).then((tabs) => {
			browser.pageAction.show(tabs[0].id);
		});
	}
});
